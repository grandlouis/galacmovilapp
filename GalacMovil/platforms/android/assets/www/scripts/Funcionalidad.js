﻿document.addEventListener('deviceready', OnDeviceReady, false);
function OnDeviceReady() {
    var _InAppBrowserRef;
    var _Usuario;
    const _Reportes = "Reportes";
    const _Configuracion = "Configuracion";
    var _PaginaActual;
    var _PaginaVisible = false;
    function UrlPagina(valNombre) {
        switch (valNombre) {
            case _Reportes:
                return 'https://movil.galac.com:30/';
            case _Configuracion:
                return 'https://movilconfiguracion.galac.com:40/Usuario/InicioSesion/';
            default:
                return '';
        }
    }

    function Entrar(valPagina) {
        CerrarVentana(_InAppBrowserRef);
        CargarUsuario(function (vUsuario) {
            $('#chequeando-usuario').hide();
            if (vUsuario !== null && vUsuario["Correo"] !== "" && vUsuario["Correo"] !== undefined) {
                IniciarSesion(valPagina, vUsuario);
            } else {
                MostrarLogin();
            }
        });
    }

    Entrar(_Reportes);

    $('#menu-btn-web').click(function () {
        Entrar(_Configuracion);
    });

    $('#menu-btn-reportes').click(function () {
        Entrar(_Reportes);
    });

    $('#menu-btn-cerrar').click(function () {
        SalirDeUsuario(true);
    });

    $('#btn-login').click(function () {
        var vUsuario = $('#input-usuario').val();
        var vClave = $('#input-clave').val();
        var vRecordar = $('#check-recordarme').prop("checked");

        GuardarUsuario(vUsuario, vClave, vRecordar);
        IniciarSesion(_Reportes, _Usuario);

    });

    function CargarPagina(valPagina, valCallback) {
        _PaginaActual = valPagina;
        var vOpciones =
            'location=no' +
            ',zoom=no' +
            ',hidden=yes';
        if (_InAppBrowserRef !== undefined) {
            CerrarVentana(_InAppBrowserRef);
        }
        var vUrl = UrlPagina(valPagina);
        _InAppBrowserRef = cordova.InAppBrowser.open(vUrl, '_blank', vOpciones);

        _InAppBrowserRef.addEventListener('loadstop', function () {
            if (_InAppBrowserRef !== undefined) {
                if (valCallback !== undefined) {
                    valCallback(_InAppBrowserRef);
                } else {
                    MostrarMenu();
                    MostrarVentana(_InAppBrowserRef);
                }
            }
        });
        _InAppBrowserRef.addEventListener('loaderror', function () {
            if (_InAppBrowserRef !== undefined) {
                CerrarVentana(_InAppBrowserRef);
            }
            alert(
                "Ocurrio un error cargando la pagina, " +
                "por favor verifique su conexión a internet " +
                "e intentelo de nuevo.\n\nSi el error persiste, " +
                "intentelo de nuevo mas tarde."
                );
        });
    }

    function MostrarMenu() {
        $('#menu').show();
        $('#conectando').hide();
        $('#login').hide();
        $('#titulo').removeClass("blink");
        $('.app').removeClass('cargando');
    }

    function MostrarLogin() {
        $('#login').show();
        $('#conectando').hide();
        $('#menu').hide();
        $('#titulo').removeClass("blink");
        $('.app').removeClass('cargando');
    }

    function Cargando() {
        $('#conectando').html("Conectando...");
        $('#conectando').show();
        $('#menu').hide();
        $('#login').hide();
        $('#titulo').addClass("blink");
        $('.app').addClass('cargando');
    }

    function MostrarVentana(valVentana) {
        valVentana.show();
        _PaginaVisible = true;
    }

    function OcultarVentana(valVentana) {
        valVentana.hide();
        _PaginaVisible = false;
    }

    function CerrarVentana(valVentana) {
        try {
            valVentana.close();
        } catch (e) {
            console.log("no window opened");
        }
        _PaginaVisible = false;
        valVentana = undefined;
        _InAppBrowserRef = undefined;
    }

    function IniciarSesion(valPagina, valUsuario) {
        Cargando();
        CargarPagina(valPagina, function (_InAppBrowserRef) {
            $('#conectando').html("Obteniendo cuentas...");
            var vSolicitudPagina = _PaginaVisible ? "try{Pagina}catch(e){null}" : "try{Pagina}catch(e){VerificarLogin()}";
            _InAppBrowserRef.executeScript({
                code: "function VerificarLogin() {"
                                    + "try {"
                                        + "var a = $('span.text-danger');"
                                        + "var vError = $(a[a.length - 1]).html();"
                                        + "if (vError != \"\" && vError !== undefined) {"
                                        + "     return \"Datos Incorrectos\";"
                                        + "} else if ($('.cuadro-mensaje-error').html() !== undefined) {"
                                        + "     return \"Datos Incorrectos\";"
                                        + "}"
                                        + "throw 'error';"
                                    + "} catch (e) {"
                                        + "throw e;"
                                    + "}"
                                  + "}" +
                      "try{VerificarLogin()}catch(e){$('#Usuario').val('" + valUsuario["Correo"] + "');" +
                      "$('#Clave').val('" + valUsuario['Clave'] + "');" +
                      "$('input[type=\"submit\"]').click();}" +
                      vSolicitudPagina
            }, function (values) {
                try {
                    if (values !== undefined && values[0] !== null && values[0] !== "") {
                        if (values[0] === "Datos Incorrectos") {
                            CerrarVentana(_InAppBrowserRef);
                            MostrarLogin();
                            alert("Usuario o contraseña incorrectos");
                        } else {
                            MostrarVentana(_InAppBrowserRef);//
                            MostrarMenu();
                        }
                    }
                    else {
                        CerrarVentana(_InAppBrowserRef);
                        MostrarMenu();
                    }
                } catch (e) {
                    CerrarVentana(_InAppBrowserRef);
                    MostrarMenu();
                }
            });
        });
    }

    function GuardarUsuario(valUsuario, valClave, valRecordar) {
        _Usuario = {
            Correo: valUsuario,
            Clave: valClave
        };
        if (valRecordar) {
            var db = window.sqlitePlugin.openDatabase({ name: 'GalacMovil.db', location: 'default' });
            db.sqlBatch([
                'DROP TABLE IF EXISTS Usuario',
                'CREATE TABLE IF NOT EXISTS Usuario (correo, clave)',
                ['INSERT INTO Usuario VALUES (?,?)', [valUsuario, valClave]]
            ], function () {
                //alert('Populated database OK');
            }, function (error) {
                //alert('SQL batch ERROR: ' + error.message);
            });
        }
        else {
            SalirDeUsuario(false);
        }
    }

    function CargarUsuario(valCallback) {
        if (_Usuario !== undefined) {
            valCallback(_Usuario);
        } else {
            var db = window.sqlitePlugin.openDatabase({ name: 'GalacMovil.db', location: 'default' });
            var vResult = {};
            db.executeSql('SELECT correo,clave FROM Usuario', [], function (rs) {
                try {
                    vResult["Correo"] = rs.rows.item(0)["correo"];
                    vResult["Clave"] = rs.rows.item(0)["clave"];
                } catch (e) {
                    vResult = null;
                }
                valCallback(vResult);
            }, function (error) {
                valCallback(vResult);
            });
        }
    }

    function SalirDeUsuario(valMostrarLogin) {
        var db = window.sqlitePlugin.openDatabase({ name: 'GalacMovil.db', location: 'default' });
        db.sqlBatch([
            'DROP TABLE IF EXISTS Usuario'
        ], function () {
            if (valMostrarLogin) {
                MostrarLogin();
            }
        }, function (error) {
            //alert('Ocurrio un error al cerrar sesión');
        });
    }
}